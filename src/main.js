/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import 'mutationobserver-shim'
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import './plugins/bootstrap-vue'
import './plugins/vee-validate'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'
import i18n from './vue-i18n'

Vue.config.productionTip = false

Vue.use(VueI18n)

new Vue({
  router,
  store,
  i18n,
  render: h => h(App)
}).$mount('#app')
