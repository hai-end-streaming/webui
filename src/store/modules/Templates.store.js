/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/

export const namespaced = true

export const state = {
  hymnList: [],
  templateList: []
}

export const actions = {
  loadTemplates ({ commit }) {
    commit('LOAD_TEMPLATE_LIST')
  },
  addTemplate ({ commit }, template) {
    console.log('addTemplate', template)
    commit('ADD_TEMPLATE', template)
    commit('SAVE_TEMPLATE_LIST')
  },
  removeTemplate ({ commit }, index) {
    commit('REMOVE_TEMPLATE', index)
    commit('SAVE_TEMPLATE_LIST')
  },
  loadHymns ({ commit }) {
    commit('LOAD_HYMN_LIST')
  },
  addHymn ({ commit }, hymn) {
    commit('ADD_HYMN', hymn)
    commit('SAVE_HYMN_LIST')
  },
  removeHymn ({ commit }, index) {
    commit('REMOVE_HYMN', index)
    commit('SAVE_HYMN_LIST')
  }
}

export const getters = {
  templateList: (state) => {
    return state.templateList
  },
  hymnList: (state) => {
    return state.hymnList
  }
}

export const mutations = {
  LOAD_TEMPLATE_LIST (state) {
    const templates = localStorage.getItem('haiend.text-templates')
    if (templates) {
      state.templateList = JSON.parse(templates)
    } else {
      state.templateList = []
    }
  },
  SAVE_TEMPLATE_LIST (state) {
    if (state.templateList.length > 0) {
      window.localStorage.setItem('haiend.text-templates', JSON.stringify(state.templateList))
    } else {
      window.localStorage.removeItem('haiend.text-templates')
    }
  },
  ADD_TEMPLATE (state, payload) {
    console.log('ADD_TEMPLATE', payload)
    if (payload) {
      state.templateList.push(payload)
    }
  },
  REMOVE_TEMPLATE (state, index) {
    state.templateList.splice(index, 1)
  },
  LOAD_HYMN_LIST (state) {
    const templates = localStorage.getItem('haiend.hymn-templates')
    if (templates) {
      state.hymnList = JSON.parse(templates)
    } else {
      state.hymnList = []
    }
  },
  SAVE_HYMN_LIST (state) {
    if (state.hymnList.length > 0) {
      window.localStorage.setItem('haiend.hymn-templates', JSON.stringify(state.hymnList))
    } else {
      window.localStorage.removeItem('haiend.hymn-templates')
    }
  },
  ADD_HYMN (state, payload) {
    state.hymnList.push(payload)
  },
  REMOVE_HYMN (state, index) {
    state.hymnList.splice(index, 1)
  }
}
