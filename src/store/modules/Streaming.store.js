/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
export const namespaced = true

export const state = {
  streamingState: null,
  streamingError: null,
  bitrate: null,
  resolution: null
}

export const actions = {
  setStreamingState ({ commit, state }, streamingState) {
    commit('SET_STREAMING_STATE', streamingState)
  },
  setStreamingError ({ commit, state }, streamingError) {
    commit('SET_STREAMING_ERROR', streamingError)
  },
  setBitrate ({ commit, state }, bitrate) {
    commit('SET_BITRATE', bitrate)
  },
  setResolution ({ commit, state }, resolution) {
    commit('SET_RESOLUTION', resolution)
  }
}

export const getters = {
  streamingState: (state) => {
    return state.streamingState
  },
  streamingError: (state) => {
    return state.streamingError
  },
  bitrate: (state) => {
    return state.bitrate
  },
  resolution: (state) => {
    return state.resolution
  }
}

export const mutations = {
  SET_STREAMING_STATE (state, streamingState) {
    state.streamingState = streamingState
  },
  SET_STREAMING_ERROR (state, streamingError) {
    state.streamingError = streamingError
  },
  SET_BITRATE (state, bitrate) {
    state.bitrate = bitrate
  },
  SET_RESOLUTION (state, resolution) {
    state.resolution = resolution
  }
}
