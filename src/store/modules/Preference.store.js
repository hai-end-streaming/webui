/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import preferenceService from '@/services/preference.service'
import store from '@/store'
export const namespaced = true

export const state = {
  isLoading: false,
  preferences: null
}

export const mutations = {
  SET (state, pref) {
    if (state.preferences === null) {
      state.preferences = []
    }
    state.preferences[pref.key] = pref.value
  },
  SET_ALL (state, preferences) {
    state.preferences = preferences
  },
  SAVE (state) {
    preferenceService.save(state.preferences)
  },
  LOAD (state) {
    state.isLoading = true
    state.preferences = preferenceService.load()
    state.isLoading = false
  }
}

export const actions = {
  set ({ commit }, payload) {
    commit('SET', { key: payload.key, value: payload.value })
    commit('SAVE')
  },
  setAll ({ commit }, preferences) {
    commit('SET_ALL', { preferences })
    commit('SAVE')
  },
  load ({ commit }) {
    commit('LOAD')
  }
}

export const getters = {
  get: (state) => (key, defaultValue) => {
    if (!state.isLoading && !state.preferences) {
      store.dispatch('Preference/load')
    }
    if (state.preferences[key]) {
      return state.preferences[key]
    } else {
      return defaultValue
    }
  }
}
