/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'

// import the auto exporter
import modules from './modules'

Vue.use(Vuex)
const debug = false // process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules, // all your modules automatically imported :)
  // Making sure that we're doing
  // everything correctly by enabling
  // strict mode in the dev environment.
  strict: process.env.NODE_ENV !== 'production',
  plugins: debug ? [createLogger()] : []
})
