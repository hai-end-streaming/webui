/*
  SPDX-License-Identifier: AGPL-3.0-or-later
  Hai-End Streaming - Streaming Server Frontend
  Copyright (C) 2021 Stephan Strittmatter
*/
const webpack = require('webpack')
const LicenseCheckerWebpackPlugin = require('license-checker-webpack-plugin')

module.exports = {
  lintOnSave: false,
  configureWebpack: {
    devtool: 'source-map',
    plugins: [
      // janus.js does not use 'import' to access to the functionality of webrtc-adapter,
      // instead it expects a global object called 'adapter' for that.
      // Let's make that object available.
      new webpack.ProvidePlugin({
        adapter: ['webrtc-adapter', 'default']
      }),
      new LicenseCheckerWebpackPlugin({
        outputFilename: 'ThirdPartyNotices.txt',
        ignore: [
          'janus-gateway', // AGPLv3 is compatible with GPLv3
          'vue-lang-code-flags' // It is MIT. wrong license within package.json
        ]
      })
    ],
    module: {
      rules: [
        // janus.js does not use 'export' to provide its functionality to others, instead
        // it creates a global variable called 'Janus' and expects consumers to use it.
        // Let's use 'exports-loader' to simulate it uses 'export'.
        {
          test: require.resolve('janus-gateway'),
          loader: 'exports-loader',
          options: {
            exports: 'Janus'
          }
        }
      ]
    }
  },
  pwa: {
    name: 'Hai-End Streaming',
    themeColor: '#5793c9',
    msTileColor: '#5793c9',
    appleMobileWebAppCapable: 'yes',
    appleMobileWebAppStatusBarStyle: 'black-translucent',
    manifestCrossorigin: 'anonymous',
    manifestOptions: {
      background_color: '#5793c9'
    },
    // configure the workbox plugin
    workboxPluginMode: 'InjectManifest',
    workboxOptions: {
      swSrc: 'src/service-worker.js'
    }
  }
}
